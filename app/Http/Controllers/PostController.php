<?php

namespace App\Http\Controllers;

use App\Http\Requests\PollDeleteRequest;
use App\Http\Requests\PollStoreRequest;
use App\Http\Requests\PollUpdateRequest;
use App\Http\Requests\PostDeleteRequest;
use App\Http\Requests\PostStoreRequest;
use App\Http\Requests\PostUpdateRequest;
use App\Poll;
use App\Post;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function one()
    {
        try {
            return response()->json(
                Post::query()
                    ->findOrFail(request('id'))
            );
        } catch (ModelNotFoundException $exception) {
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(404);
        }

    }

    /**
     * Deletes the post.
     *
     * @param PostDeleteRequest $request - Request object
     * @return JsonResponse
     */
    public function delete(PostDeleteRequest $request)
    {
        return $request->persist();
    }

    public function update(PostUpdateRequest $request)
    {
        return $request->persist();
    }

    /**
     * Fetches all polls.
     *
     * @return JsonResponse
     */
    public function all()
    {
        return response()->json(Post::all()->load('bookmarks'));
    }

    /**
     * Stores a poll and its options to
     * the database.
     *
     * @param PostStoreRequest $request - Request object
     * @return JsonResponse
     */
    public function store(PostStoreRequest $request)
    {
        return $request->persist();
    }
}
