<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\Http\Requests\UserUpdateRequest;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /**
     * Create user.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function signUp(Request $request)
    {
        $request->validate([
            'phone' => 'required|string',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|string|email|unique:users,email',
            'password' => 'required|string|confirmed',
        ]);

        $user = new User([
            'phone' => $request->get('phone'),
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'company' => $request->get('company'),
            'position' => $request->get('position'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password'))
        ]);

        $user->save();

        return response()->json([
            'message' => 'Successfully created user!'
        ], 201);
    }

    /**
     * Login user and create token
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function logIn(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);

        $credentials = request(['email', 'password']);

        if (!auth()->attempt($credentials)) {
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        }

        $user = $request->user();

        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;

        if ($request->get('remember_me')) {
            $token->expires_at = Carbon::now()->addWeeks(1);
        }

        $token->save();

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }

    /**
     * Logout user (Revoke the token)
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function logOut(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    public function one()
    {
        try {
            return response()->json(
                User::query()->findOrFail(request('id'))
            );
        } catch (ModelNotFoundException $exception) {
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(404);
        }

    }

    public function me()
    {
        /** @var User $user */
        $user = auth()->user();

        return response()->json($user);
    }

    /**
     * Get the authenticated User
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }

    public function update(UserUpdateRequest $request)
    {
        return $request->persist();
    }

    public function validateUser(Request $request, int $stage)
    {
        switch ($stage) {
            case 1:
                $request->validate([
                    'email' => 'required|string|email|unique:users,email',
                ]);
                break;
            case 2:
                $request->validate([
                    'phone' => 'required|string|unique:users,phone',
                    'first_name' => 'required|string',
                    'last_name' => 'required|string',
                    'company' => 'nullable|string',
                    'position' => 'nullable|string',
                ]);
                break;
            case 3:
                $request->validate([
                    'phone' => 'required|string|unique:users,phone',
                    'first_name' => 'required|string',
                    'last_name' => 'required|string',
                    'company' => 'nullable|string',
                    'position' => 'nullable|string',
                ]);
                break;
        }
    }
}
