<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use App\PasswordReset;
use Illuminate\Support\Facades\Hash;
use Jenssegers\Agent\Agent;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Send a reset link to the given user.
     *
     * @param Request $request
     * @return RedirectResponse|JsonResponse
     */
    public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);

        $response = $this->broker()->sendResetLink(
            $this->credentials($request)
        );

        return response()->json(array(
            'result' => trans($response),
        ), $response == Password::RESET_LINK_SENT ? 200 : 500);
    }

    public function recovery(string $token)
    {
        $agent = new Agent();
        $email = '';

        foreach (PasswordReset::all() as $item) {
            if (Hash::check($token, $item->getAttribute('token'))) {
                $email = $item->getAttribute('email');
            }
        }

        if ($agent->isMobile()) {
            return response()
                ->redirectTo('mozhayskaya://password/' . $token . '/' . $email);
        }

        return response()
            ->redirectTo('/password/reset/' . $token . '/' . $email);
    }
}
