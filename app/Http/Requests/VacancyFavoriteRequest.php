<?php

namespace App\Http\Requests;

use App\Vacancy;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Http\FormRequest;

class VacancyFavoriteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            //
        ];
    }

    public function persist()
    {
        try {
            /** @var Vacancy $vacancy */
            $vacancy = Vacancy::query()->findOrFail($this->route('id'));

            $vacancy->users()->toggle(auth()->id());
            $vacancy->save();

            return response()->json($vacancy->load('conditions'));
        } catch (ModelNotFoundException $exception) {
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(404);
        } catch (Exception $exception) {
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(500);
        }
    }
}
