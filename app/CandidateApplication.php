<?php

namespace App;

// Framework
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class CandidateApplication extends Model
{
    use SoftDeletes;

    protected $fillable = ['status'];

    /**
     * Returns the candidate who
     * applied.
     *
     * @return BelongsTo
     */
    public function candidate(): BelongsTo
    {
        return $this->belongsTo('App\Candidate');
    }

    /**
     * Returns the user who sent
     * the application.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo('App\User');
    }
}
