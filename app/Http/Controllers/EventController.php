<?php

namespace App\Http\Controllers;

use App\Event;
use App\Http\Requests\EventDeleteRequest;
use App\Http\Requests\EventStoreRequest;
use App\Http\Requests\EventUpdateRequest;
use App\User;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;

class EventController extends Controller
{
    /**
     * Returns all events.
     *
     * @return JsonResponse
     */
    public function all()
    {
        return response()->json(
            Event::all()
                ->load(['experts', 'users', 'settings'])
                ->sortByDesc('date_time')
                ->values()
        );
    }

    public function one()
    {
        try {
            return response()->json(
                Event::query()->findOrFail(request('id'))
                    ->load(['experts', 'users', 'settings'])
            );
        } catch (ModelNotFoundException $exception) {
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(404);
        }
    }

    /**
     * Stores an event.
     *
     * @param EventStoreRequest $request
     * @return JsonResponse
     */
    public function store(EventStoreRequest $request)
    {
        return $request->persist();
    }

    /**
     * Deletes the event.
     *
     * @param EventDeleteRequest $request
     * @return JsonResponse
     */
    public function delete(EventDeleteRequest $request)
    {
        return $request->persist();
    }

    /**
     * Updates the event.
     *
     * @param EventUpdateRequest $request
     * @return JsonResponse
     */
    public function update(EventUpdateRequest $request)
    {
        return $request->persist();
    }

    /**
     * Attaches the user to the event.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function signUp(int $id): JsonResponse
    {
        try {
            /** @var User $user */
            $user = auth()->user();

            /** @var Event $event */
            $event = Event::query()->findOrFail($id);

            if ($event->getAttribute('is_applied')) {
                $event->users()->detach($user->getAttribute('id'));
            } else {
                $event->users()->sync([
                    $user->getAttribute('id') => ['role' => 0],
                ]);
            }

            return response()->json($event->load(['experts', 'settings', 'users']));
        } catch (ModelNotFoundException $exception) { // Catching 404s
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(404);
        } catch (Exception $exception) { // Catching other errors
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(500);
        }
    }
}
