<?php

namespace App\Http\Controllers;

use App\Condition;
use App\Http\Requests\ConditionStoreRequest;
use App\Http\Requests\ConditionUpdateRequest;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;

class ConditionController extends Controller
{
    public function all()
    {
        return response()->json(Condition::all());
    }

    public function one()
    {
        try {
            return response()->json(
                Condition::query()->findOrFail(request('id'))
            );
        } catch (ModelNotFoundException $exception) {
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(404);
        }
    }

    /**
     * Stores a condition.
     *
     * @param ConditionStoreRequest $request
     * @return JsonResponse
     */
    public function store(ConditionStoreRequest $request): JsonResponse
    {
        return $request->persist();
    }

    public function update(ConditionUpdateRequest $request): JsonResponse
    {
        return $request->persist();
    }

    public function delete(): JsonResponse
    {
        try {
            $condition = Condition::query()->findOrFail(request('id'));
            return response()->json($condition->delete());
        } catch (ModelNotFoundException $exception) {
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(404);
        } catch (Exception $exception) {
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(500);
        }
    }
}
