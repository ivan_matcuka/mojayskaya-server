<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class VacancyApplication extends Model
{
    use SoftDeletes;

    protected $fillable = ['status'];

    /**
     * Returns the vacancy this application
     * was sent for.
     *
     * @return BelongsTo
     */
    public function vacancy()
    {
        return $this->belongsTo('App\Vacancy');
    }

    /**
     * Returns the user who sent
     * the application.
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
