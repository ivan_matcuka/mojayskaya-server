<?php

namespace App\Http\Requests;

use App\Option;
use App\Poll;
use App\Post;
use Carbon\Carbon;
use Exception;
use Illuminate\Foundation\Http\FormRequest;

class PostStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject' => 'required|string',
            'teaser' => 'required|string',
            'illustration' => 'string',
            'content' => 'required|string',
        ];
    }

    public function persist()
    {
        try {
            $post = new Post($this->all());
            $post->save();

            return response()->json($post);
        } catch (Exception $exception) {
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(500);
        }
    }
}
