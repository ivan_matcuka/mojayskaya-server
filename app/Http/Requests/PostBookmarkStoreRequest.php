<?php

namespace App\Http\Requests;

use App\PostBookmark;
use Exception;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class PostBookmarkStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'integer|required',
            'post_id' => 'integer|required',
        ];
    }

    /**
     * Persists the request.
     *
     * @return JsonResponse
     */
    public function persist(): JsonResponse
    {
        try {
            $postBookmark = PostBookmark::query()
                ->where([
                    ['user_id', '=', $this->get('user_id')],
                    ['post_id', '=', $this->get('post_id')],
                ])
                ->first();

            if ($postBookmark) {
                throw new Exception('Bookmark already exists');
            }

            $postBookmark = new PostBookmark($this->all());
            $postBookmark->save();

            return response()->json($postBookmark->load('post'));
        } catch (Exception $exception) {
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(500);
        }
    }
}
