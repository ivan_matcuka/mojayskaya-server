<?php

namespace App;

// Framework
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Candidate extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'position',
        'salary',
        'description',
        'for_whom',
        'skills',
        'user_id',
        'created_at',
    ];

    protected $appends = ['is_applied', 'is_favorite', 'type'];

    /**
     * Returns conditions specified
     * by the candidate.
     *
     * @return BelongsToMany
     */
    public function conditions(): BelongsToMany
    {
        return $this->belongsToMany('App\Condition', 'condition_candidate');
    }

    /**
     * Returns all the application vacancies.
     *
     * @return HasMany
     */
    public function applications(): HasMany
    {
        return $this->hasMany('App\CandidateApplication');
    }

    /**
     * Returns the user attached to
     * the candidate.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo('App\User');
    }

    public function getIsAppliedAttribute(): bool
    {
        return $this->applications()
            ->pluck('user_id')
            ->contains(auth()->id());
    }

    public function getIsFavoriteAttribute(): bool
    {
        return $this->users()
            ->pluck('user_id')
            ->contains(auth()->id());
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany('App\User', 'user_candidates');
    }

    /**
     * Gets the "type" field.
     *
     * @return string
     */
    public function getTypeAttribute(): string
    {
        return 'candidate';
    }
}
