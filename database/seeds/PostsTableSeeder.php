<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        // Without illustration
        DB::table('posts')->insert([
            'subject' => 'Post #1',
            'teaser' => $faker->sentence(4, true),
            'content' => $faker->randomHtml(15),
            'created_at' => $faker->dateTimeBetween('-6 days', '-3 days'),
        ]);

        // With illustration
        DB::table('posts')->insert([
            'subject' => 'Post #2',
            'teaser' => $faker->sentence(4, true),
            'illustration' => $faker->imageUrl(640, 480, 'cats'),
            'content' => $faker->randomHtml(15),
            'created_at' => $faker->dateTimeBetween('-6 days', '-3 days'),
        ]);
    }
}
