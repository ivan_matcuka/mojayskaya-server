<?php

namespace App\Http\Requests;

use App\Option;
use App\Poll;
use Carbon\Carbon;
use Exception;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class PollStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject' => 'required|string',
            'teaser' => 'string|nullable',
            'options' => 'required|array',
            'active' => 'boolean',
            'start' => 'required|string',
            'end' => 'required|string',
        ];
    }

    /**
     * Persist the request and returns
     * the result as JSON object.
     *
     * @return JsonResponse
     */
    public function persist()
    {
        try {
            /**
             * Create a new poll
             */
            $poll = new Poll([
                'subject' => $this->get('subject'),
                'teaser' => $this->get('teaser'),
                'active' => (bool)$this->get('active'),
                'start' => Carbon::parse($this->get('start')),
                'end' => Carbon::parse($this->get('end')),
            ]);

            /**
             * Save it as we need its ID in order
             * to attach options
             */
            $poll->save();

            foreach ($this->get('options') as $option) {
                $option = (new Option(['text' => $option]));
                $option->timestamps = false;
                $poll->options()->save($option);
            }

            /**
             * Options eager loading
             */
            $poll->load('options');

            return response()->json($poll);
        } catch (Exception $exception) {
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(500);
        }
    }
}
