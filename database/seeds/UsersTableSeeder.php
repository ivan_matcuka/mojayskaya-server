<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')
            ->insert([
                'email' => 'test@test.ru',
                'password' => bcrypt('qwerty'),
                'first_name' => 'Test',
                'last_name' => 'Tester',
                'company' => 'Testing',
                'position' => 'Tester',
                'phone' => '+7 (999) 999-99-99',
                'role' => 0,
            ]);
        DB::table('users')
            ->insert([
                'email' => 'matcuka@polymorphy.ru',
                'password' => bcrypt('qwerty'),
                'first_name' => 'Иван',
                'last_name' => 'Мацука',
                'company' => 'Partnersheep',
                'position' => 'CEO',
                'phone' => '+7 (915) 347-33-81',
                'role' => 1,
            ]);
        DB::table('users')
            ->insert([
                'email' => 'evanandrewmathews@gmail.com',
                'password' => bcrypt('qwerty'),
                'first_name' => 'Evan',
                'last_name' => 'Mathews',
                'company' => 'Mojayskaya',
                'position' => 'Web Developer',
                'phone' => '+7 (916) 845-84-60',
                'role' => 2,
            ]);
    }
}
