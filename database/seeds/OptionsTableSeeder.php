<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        DB::table('options')->insert([
            'text' => $faker->sentence(3, true),
            'poll_id' => 1,
        ]);
        DB::table('options')->insert([
            'text' => $faker->sentence(3, true),
            'poll_id' => 1,
        ]);
        DB::table('options')->insert([
            'text' => $faker->sentence(3, true),
            'poll_id' => 1,
        ]);


        DB::table('options')->insert([
            'text' => $faker->sentence(3, true),
            'poll_id' => 2,
        ]);
        DB::table('options')->insert([
            'text' => $faker->sentence(3, true),
            'poll_id' => 2,
        ]);


        DB::table('options')->insert([
            'text' => $faker->sentence(3, true),
            'poll_id' => 3,
        ]);
        DB::table('options')->insert([
            'text' => $faker->sentence(3, true),
            'poll_id' => 3,
        ]);
        DB::table('options')->insert([
            'text' => $faker->sentence(3, true),
            'poll_id' => 3,
        ]);


        DB::table('options')->insert([
            'text' => $faker->sentence(3, true),
            'poll_id' => 4,
        ]);
        DB::table('options')->insert([
            'text' => $faker->sentence(3, true),
            'poll_id' => 4,
        ]);
        DB::table('options')->insert([
            'text' => $faker->sentence(3, true),
            'poll_id' => 4,
        ]);
        DB::table('options')->insert([
            'text' => $faker->sentence(3, true),
            'poll_id' => 4,
        ]);
    }
}
