<?php

namespace App;

use App\Notifications\PasswordResetRequest;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $fillable = [
        'name',
        'email',
        'password',
        'first_name',
        'last_name',
        'gender',
        'company',
        'position',
        'facebook',
        'phone',
        'picture',
        'cv',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'pivot'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordResetRequest($token));
    }

    /**
     * Returns the events this user
     * ever hosted.
     *
     * @return HasMany
     */
    public function events()
    {
        return $this->hasMany('App\Event');
    }

    /**
     * Returns the candidate attached
     * to the user.
     *
     * @return HasOne
     */
    public function candidate()
    {
        return $this->hasOne('App\Candidate');
    }

    /**
     * Returns the candidate application
     * sent by this user.
     *
     * @return HasMany
     */
    public function candidateApplications()
    {
        return $this->hasMany('App\CandidateApplication');
    }

    /**
     * Returns the vacancy applications
     * this user sent.
     *
     * @return HasMany
     */
    public function vacancyApplications()
    {
        return $this->hasMany('App\VacancyApplication');
    }

    /**
     * Returns the poll responses this
     * user sent.
     *
     * @return HasMany
     */
    public function pollResponses()
    {
        return $this->hasMany('App\PollResponses');
    }

    /**
     * Returns this user's bookmarks.
     *
     * @return HasMany
     */
    public function bookmarks()
    {
        return $this->hasMany('App\Bookmark');
    }

    public function vacancies(): BelongsToMany
    {
        return $this->belongsToMany('App\Vacancy', 'user_vacancies');
    }

    public function candidates(): BelongsToMany
    {
        return $this->belongsToMany('App\Candidate', 'user_candidates');
    }
}
