<?php

namespace App\Http\Requests;

use App\Candidate;
use App\Condition;
use App\Option;
use App\Poll;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Http\FormRequest;

class CandidateStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'position' => 'required|string',
            'salary' => 'required|string',
            'description' => 'required|string',
            'for_whom' => 'required|string',
            'skills' => 'required|string',
            'user_id' => 'integer',
            'conditions' => 'array',
        ];
    }

    public function persist()
    {
        try {
            $user = User::query()->find($this->get('user_id'));

            /**
             * Create a new poll
             */
            $candidate = new Candidate([
                'position' => $this->get('position'),
                'salary' => $this->get('salary'),
                'description' => $this->get('description'),
                'for_whom' => $this->get('for_whom'),
                'skills' => $this->get('skills'),
            ]);

            if ($user) {
                $candidate->user()->associate($user);
            }

            /**
             * Save it as we need its ID in order
             * to attach conditions
             */
            $candidate->save();

            if ($this->get('conditions')) {
                foreach ($this->get('conditions') as $conditionId) {
                    $condition = Condition::query()->findOrFail($conditionId);

                    if ($condition) {
                        $candidate->conditions()->attach($conditionId);
                    }
                }
            }

            /**
             * Options eager loading
             */
            $candidate->load('conditions');

            return response()->json($candidate);
        } catch (ModelNotFoundException $exception) {
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(404);
        } catch (Exception $exception) {
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(500);
        }
    }
}
