<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Events
        Schema::create('events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('date_time');
            $table->string('subject');
            $table->text('teaser');
            $table->longText('illustration')
                ->nullable();
            $table->text('description');
            $table->string('address');
            $table->bigInteger('settings_id')->unsigned();
            $table->foreign('settings_id')
                ->references('id')
                ->on('event_settings')
                ->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });

        // Experts
        Schema::create('event_user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('event_id')->unsigned();
            $table->tinyInteger('role')->unsigned()->default(0);

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->foreign('event_id')
                ->references('id')
                ->on('events')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_user');
        Schema::dropIfExists('events');
    }
}
