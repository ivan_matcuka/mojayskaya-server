<?php

namespace App\Http\Requests;

use App\Candidate;
use App\Condition;
use App\User;
use App\Vacancy;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Http\FormRequest;

class VacancyUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'position' => 'required|string',
            'salary' => 'required|string',
            'description' => 'required|string',
            'tasks' => 'required|string',
            'requirements' => 'required|string',
            'conditions' => 'array',
        ];
    }

    public function persist()
    {
        try {
            /** @var Vacancy $vacancy */
            $vacancy = Vacancy::query()->findOrFail($this->route('id'));
            $vacancy->conditions()->detach();

            foreach ($this->get('conditions') as $conditionId) {
                $condition = Condition::query()->find($conditionId);

                if ($condition) {
                    $vacancy->conditions()->attach($conditionId);
                }
            }

            /**
             * Options eager loading
             */
            $vacancy->load('conditions');
            $vacancy->update($this->all());

            return response()->json($vacancy);
        } catch (ModelNotFoundException $exception) {
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(404);
        } catch (Exception $exception) {
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(500);
        }
    }
}
