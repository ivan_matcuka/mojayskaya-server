<?php

namespace App\Http\Requests;

// App
use App\Event;
use App\EventSettings;
use App\User;

// Libs
use Carbon\Carbon;

// PHP
use Exception;

// Framework
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class EventStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'date_time' => 'required|date',
            'subject' => 'required|string',
            'teaser' => 'required|string',
            'illustration' => 'mimes:jpeg,jpg,png,gif',
            'description' => 'required|string',
            'address' => 'required|string',
            'limit' => 'required|integer',
            'type' => 'required|integer',
            'valid_til' => 'date',
        ];
    }

    /**
     * Saves new settings.
     *
     * @param Event $event - Event instance to store.
     */
    private function saveSettings(Event $event): void
    {
        $validTil = $this->get('valid_til') ?? $this->get('date_time');

        $settings = new EventSettings([
            'limit' => $this->get('limit'),
            'type' => $this->get('type'),
            'valid_til' => Carbon::parse($validTil),
        ]);

        $settings->timestamps = false;
        $settings->save();

        $event->settings()->associate($settings);
    }

    /**
     * Saves experts.
     * Links the event to users.
     *
     * @param Event $event - Event instance to store.
     */
    private function saveExperts(Event $event): void
    {
        foreach ($this->get('experts') as $expertId) {
            /** @var User $expert */
            $expert = User::query()->findOrFail($expertId);

            $event->users()->sync([
                $expert->getAttribute('id') => ['role' => 1],
            ]);
        }
    }

    /**
     * Persists the request.
     *
     * @return JsonResponse
     */
    public function persist(): JsonResponse
    {
        try {
            $event = new Event([
                'date_time' => Carbon::parse($this->get('date_time')),
                'subject' => $this->get('subject'),
                'teaser' => $this->get('teaser'),
                'illustration' => $this->file('illustration'),
                'description' => $this->get('description'),
                'address' => $this->get('address'),
            ]);

            $this->saveSettings($event);
            $event->save();
            $this->saveExperts($event);

            return response()->json($event->load(['settings', 'users']));
        } catch (Exception $exception) {
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(500);
        }
    }
}
