<?php

namespace App\Http\Requests;

use App\User;
use Exception;
use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'string',
            'last_name' => 'string',
            'gender' => 'string|nullable',
            'company' => 'string',
            'position' => 'string',
            'facebook' => 'string|nullable',
            'phone' => 'string',
            'picture' => 'string|nullable',
            'cv' => 'string|nullable',
        ];
    }

    public function persist()
    {
        try {
            /** @var User $user */
            $user = auth()->user();
            $user->update($this->all());
            $user->update([
                'password' => bcrypt($this->get('password'))
            ]);

            return response()->json($user);
        } catch (Exception $exception) {
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(500);
        }
    }
}
