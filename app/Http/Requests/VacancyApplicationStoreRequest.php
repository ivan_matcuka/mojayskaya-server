<?php

namespace App\Http\Requests;

use App\Condition;
use App\User;
use App\Vacancy;
use App\VacancyApplication;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Http\FormRequest;

class VacancyApplicationStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status' => 'int|required',
            'user_id' => 'int|required',
            'vacancy_id' => 'int|required',
        ];
    }

    public function persist()
    {
        try {
            $vacancyApplication = VacancyApplication::query()
                ->where([
                    ['user_id', '=', $this->get('user_id')],
                    ['vacancy_id', '=', $this->get('vacancy_id')],
                ])
                ->first();

            if ($vacancyApplication) {
                throw new Exception('You already applied!');
            }

            $user = User::query()->findOrFail($this->get('user_id'));
            $vacancy = Vacancy::query()->findOrFail($this->get('vacancy_id'));

            $vacancyApplication = new VacancyApplication([
                'status' => (int)$this->get('status'),
            ]);

            $vacancyApplication->user()->associate($user);
            $vacancyApplication->vacancy()->associate($vacancy);

            $vacancyApplication->save();

            /**
             * Options eager loading
             */
            $vacancyApplication->load(['user', 'vacancy.applications', 'vacancy.conditions']);

            return response()->json($vacancyApplication);
        } catch (ModelNotFoundException $exception) {
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(404);
        } catch (Exception $exception) {
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(500);
        }
    }
}
