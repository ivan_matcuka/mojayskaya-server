<?php

namespace App\Http\Requests;

use App\Option;
use App\Poll;
use App\PollResponse;
use App\Post;
use Exception;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class PollResponseStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'user_id' => 'required|int',
            'option_id' => 'required|int',
        ];
    }

    /**
     * Persist the request.
     *
     * @return JsonResponse
     */
    public function persist(): JsonResponse
    {
        try {
            $pollResponse = PollResponse::query()
                ->where([
                    ['user_id', '=', $this->get('user_id')],
                    ['option_id', '=', $this->get('option_id')],
                ])
                ->first();

            if ($pollResponse) {
                throw new Exception('You already responded');
            }

            $pollResponse = new PollResponse($this->all());
            $pollResponse->save();

            /** @var Option $option */
            $option = Option::query()->find($this->get('option_id'));

            /** @var Poll $poll */
            $poll = Poll::query()->find($option->getAttribute('poll_id'));

            return response()->json($poll->load('options', 'responses'));
        } catch (Exception $exception) {
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(500);
        }
    }
}
