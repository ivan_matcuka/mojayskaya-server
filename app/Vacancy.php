<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vacancy extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'position',
        'salary',
        'description',
        'tasks',
        'requirements',
        'created_at',
    ];

    protected $appends = ['is_applied', 'is_favorite', 'type'];

    /**
     * Returns the conditions specified in
     * the vacancy.
     *
     * @return BelongsToMany
     */
    public function conditions(): BelongsToMany
    {
        return $this->belongsToMany('App\Condition', 'condition_vacancy');
    }

    /**
     * Returns the applications
     * for this vacancy.
     *
     * @return HasMany
     */
    public function applications(): HasMany
    {
        return $this->hasMany('App\VacancyApplication');
    }

    public function getIsAppliedAttribute(): bool
    {
        return $this->applications()
            ->pluck('user_id')
            ->contains(auth()->id());
    }

    public function getIsFavoriteAttribute(): bool
    {
        return $this->users()
            ->pluck('user_id')
            ->contains(auth()->id());
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany('App\User', 'user_vacancies');
    }

    /**
     * Gets the "type" field.
     *
     * @return string
     */
    public function getTypeAttribute(): string
    {
        return 'vacancy';
    }
}
