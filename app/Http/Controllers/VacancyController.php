<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\Http\Requests\CandidateDeleteRequest;
use App\Http\Requests\CandidateStoreRequest;
use App\Http\Requests\CandidateUpdateRequest;
use App\Http\Requests\VacancyDeleteRequest;
use App\Http\Requests\VacancyFavoriteRequest;
use App\Http\Requests\VacancyStoreRequest;
use App\Http\Requests\VacancyUpdateRequest;
use App\Vacancy;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class VacancyController extends Controller
{
    public function all()
    {
        return response()->json(
            Vacancy::all()
                ->load(['conditions'])
                ->sortBy('id')
                ->values()
        );
    }

    public function one()
    {
        try {
            return response()->json(
                Vacancy::query()->findOrFail(request('id'))
                    ->load(['applications', 'conditions'])
            );
        } catch (ModelNotFoundException $exception) {
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(404);
        }

    }

    public function store(VacancyStoreRequest $request)
    {
        return $request->persist();
    }

    public function delete(VacancyDeleteRequest $request)
    {
        return $request->persist();
    }

    public function update(VacancyUpdateRequest $request)
    {
        return $request->persist();
    }

    public function addToFavorite(VacancyFavoriteRequest $request)
    {
        return $request->persist();
    }
}
