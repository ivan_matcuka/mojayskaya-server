<?php

namespace App\Http\Requests;

use App\Candidate;
use App\Condition;
use App\User;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Http\FormRequest;

class CandidateUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'position' => 'required|string',
            'salary' => 'required|string',
            'description' => 'required|string',
            'for_whom' => 'required|string',
            'skills' => 'required|string',
            'conditions' => 'array',
        ];
    }

    public function persist()
    {
        try {
            /** @var Candidate $candidate */
            $candidate = Candidate::query()->findOrFail($this->route('id'));
            $candidate->conditions()->detach();

            foreach ($this->get('conditions') as $conditionId) {
                $condition = Condition::query()->find($conditionId);

                if ($condition) {
                    $candidate->conditions()->attach($conditionId);
                }
            }

            /**
             * Options eager loading
             */
            $candidate->load('conditions');
            $candidate->update($this->all());

            return response()->json($candidate);
        } catch (Exception $exception) {
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(500);
        }
    }
}
