<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConditionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('conditions')->insert([
            'title' => 'First condition',
        ]);
        DB::table('conditions')->insert([
            'title' => 'Second condition',
        ]);
        DB::table('conditions')->insert([
            'title' => 'Third condition',
        ]);
        DB::table('conditions')->insert([
            'title' => 'Fourth condition',
        ]);
        DB::table('conditions')->insert([
            'title' => 'Fifth condition',
        ]);
        DB::table('conditions')->insert([
            'title' => 'Sixth condition',
        ]);
        DB::table('conditions')->insert([
            'title' => 'Seventh condition',
        ]);
    }
}
