<?php

namespace App;

// Framework
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use SoftDeletes;

    public $fillable = [
        'date_time',
        'subject',
        'teaser',
        'illustration',
        'description',
        'address',
        'created_at',
    ];

    protected $appends = ['is_bookmark', 'is_applied', 'type'];

    /**
     * Returns the users this
     * event is hosted by.
     *
     * @return BelongsToMany
     */
    public function experts(): BelongsToMany
    {
        return $this->belongsToMany('App\User')->wherePivot('role', '=', 1);
    }

    /**
     * Returns the users signed up
     * for this event.
     *
     * @return BelongsToMany
     */
    public function users(): BelongsToMany
    {
        return $this->belongsToMany('App\User')->wherePivot('role', '=', 0);
    }

    /**
     * Returns the bookmarks of this event.
     *
     * @return HasMany
     */
    public function bookmarks(): HasMany
    {
        return $this->hasMany('App\EventBookmark');
    }

    /**
     * Returns the setting describing
     * the event.
     *
     * @return BelongsTo
     */
    public function settings(): BelongsTo
    {
        return $this->belongsTo('App\EventSettings');
    }

    /**
     * Gets the "is_bookmark" field.
     *
     * @return bool
     */
    public function getIsBookmarkAttribute(): bool
    {
        /** @var User $user */
        $user = auth()->user();

        if (!$user) {
            return false;
        }

        return $this->bookmarks()
            ->pluck('user_id')
            ->contains($user->getAttribute('id'));
    }

    /**
     * Gets the "is_applied" field.
     *
     * @return bool
     */
    public function getIsAppliedAttribute(): bool
    {
        return $this->users()->pluck('users.id')->contains(auth()->id());
    }

    /**
     * Gets the "type" field.
     *
     * @return string
     */
    public function getTypeAttribute(): string
    {
        return 'event';
    }
}
