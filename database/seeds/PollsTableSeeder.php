<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PollsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        // Future poll
        DB::table('polls')->insert([
            'subject' => 'Poll #1',
            'teaser' => $faker->sentence(10),
            'start' => $faker->dateTimeBetween('2 days', '5 days'),
            'end' => $faker->dateTimeBetween('6 days', '9 days'),
            'created_at' => $faker->dateTimeBetween('-6 days', '-3 days'),
        ]);

        // Current polls
        DB::table('polls')->insert([
            'subject' => 'Poll #2',
            'teaser' => $faker->sentence(10),
            'start' => $faker->dateTime('now'),
            'end' => $faker->dateTimeBetween('6 days', '9 days'),
            'created_at' => $faker->dateTimeBetween('-6 days', '-3 days'),
        ]);

        DB::table('polls')->insert([
            'subject' => 'Poll #3',
            'teaser' => $faker->sentence(10),
            'start' => $faker->dateTime('now'),
            'end' => $faker->dateTimeBetween('6 days', '9 days'),
            'created_at' => $faker->dateTimeBetween('-6 days', '-3 days'),
        ]);

        // Past polls
        DB::table('polls')->insert([
            'subject' => 'Poll #4',
            'teaser' => $faker->sentence(10),
            'start' => $faker->dateTimeBetween('-6 days', '-3 days'),
            'end' => $faker->dateTime('now'),
            'created_at' => $faker->dateTimeBetween('-6 days', '-3 days'),
        ]);
    }
}
