<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'AuthController@logIn');
    Route::post('signup', 'AuthController@signUp');
    Route::post('validate/{stage}', 'AuthController@validateUser');

    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('logout', 'AuthController@logOut');
        Route::get('user', 'AuthController@user');
    });
});

Route::group(['prefix' => 'password'], function () {
    Route::post('forgot', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::get('recovery/{token}', 'Auth\ForgotPasswordController@recovery');
    Route::post('reset', 'Auth\ResetPasswordController@reset');
});

Route::group(['middleware' => 'api'], function () {
    Route::get('feed/{page?}', 'FeedController@all');
    Route::get('polls', 'PollController@all');
    Route::get('posts', 'PostController@all');
    Route::get('posts/{id}', 'PostController@one');
    Route::get('events', 'EventController@all');
    Route::get('events/{id}', 'EventController@one');
    Route::get('conditions', 'ConditionController@all');
    Route::get('conditions/{id}', 'ConditionController@one');
    Route::get('vacancies', 'VacancyController@all');
    Route::get('vacancies/{id}', 'VacancyController@one');
    Route::get('candidates', 'CandidateController@all');
    Route::get('candidates/{id}', 'CandidateController@one');
});

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('polls', 'PollController@store');
    Route::put('polls/{id}', 'PollController@update');
    Route::delete('polls/{id}', 'PollController@delete');

    Route::post('posts', 'PostController@store');
    Route::post('posts/{id}', 'PostController@update');
    Route::delete('posts/{id}', 'PostController@delete');

    // Events
    Route::get('events/{id}/signup', 'EventController@signUp');
    Route::post('events', 'EventController@store');
    Route::post('events/{id}', 'EventController@update');
    Route::delete('events/{id}', 'EventController@delete');

    Route::post('poll-responses', 'PollResponseController@store');

    // Conditions
    Route::post('conditions', 'ConditionController@store');
    Route::post('conditions/{id}', 'ConditionController@update');
    Route::delete('conditions/{id}', 'ConditionController@delete');

    // Vacancies
    Route::post('vacancies', 'VacancyController@store');
    Route::post('vacancies/{id}', 'VacancyController@update');
    Route::delete('vacancies/{id}', 'VacancyController@delete');
    Route::get('vacancies/favorite/{id}', 'VacancyController@addToFavorite');

    // Candidates
    Route::post('candidates', 'CandidateController@store');
    Route::post('candidates/{id}', 'CandidateController@update');
    Route::delete('candidates/{id}', 'CandidateController@delete');
    Route::get('candidates/favorite/{id}', 'CandidateController@addToFavorite');

    // Vacancy Applications
    Route::get('vacancy-applications', 'VacancyApplicationController@all');
    Route::get('vacancy-applications/{id}', 'VacancyApplicationController@one');
    Route::post('vacancy-applications', 'VacancyApplicationController@store');
    Route::post('vacancy-applications/{id}', 'VacancyApplicationController@update');
    Route::delete('vacancy-applications/{id}', 'VacancyApplicationController@delete');

    // Candidate Applications
    Route::get('candidate-applications', 'CandidateApplicationController@all');
    Route::get('candidate-applications/{id}', 'CandidateApplicationController@one');
    Route::post('candidate-applications', 'CandidateApplicationController@store');
    Route::post('candidate-applications/{id}', 'CandidateApplicationController@update');
    Route::delete('candidate-applications/{id}', 'CandidateApplicationController@delete');

    // User
    Route::get('user', 'AuthController@me');
    Route::get('user/{id}', 'AuthController@one');
    Route::post('user', 'AuthController@update');

    // Bookmarks
    Route::get('bookmarks', 'BookmarkController@all');
    Route::post('bookmarks/event', 'BookmarkController@storeEventBookmark');
    Route::delete('bookmarks/event', 'BookmarkController@deleteEventBookmark');
    Route::post('bookmarks/post', 'BookmarkController@storePostBookmark');
    Route::delete('bookmarks/post', 'BookmarkController@deletePostBookmark');
});
