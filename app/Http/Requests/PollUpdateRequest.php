<?php

namespace App\Http\Requests;

use App\Option;
use App\Poll;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Http\FormRequest;

class PollUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject' => 'required|string',
            'teaser' => 'string|nullable',
            'options' => 'required|array',
            'active' => 'boolean',
            'start' => 'required|string',
            'end' => 'required|string',
        ];
    }

    public function persist()
    {
        /**
         * Trying to find the poll
         * and delete it.
         */
        try {
            /** @var Poll $poll */
            $poll = Poll::query()->findOrFail($this->route('id'));

            $poll->options()->delete();

            foreach ($this->get('options') as $option) {
                $option = (new Option(['text' => $option]));
                $option->timestamps = false;
                $poll->options()->save($option);
            }

            return response()->json($poll->update($this->all()));
        } catch (ModelNotFoundException $exception) { // Catching 404s
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(404);
        } catch (Exception $exception) { // Catching other errors
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(500);
        }
    }
}
