<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Condition extends Model
{
    use SoftDeletes;

    protected $fillable = ['title'];

    /**
     * Returns the vacancies that have this condition.
     *
     * @return BelongsToMany
     */
    public function vacancies(): BelongsToMany
    {
        return $this->belongsToMany('App\Vacancy', 'condition_vacancy');
    }

    /**
     * Returns the candidates who specified this condition.
     *
     * @return BelongsToMany
     */
    public function candidates(): BelongsToMany
    {
        return $this->belongsToMany('App\Candidate', 'condition_candidate');
    }
}
