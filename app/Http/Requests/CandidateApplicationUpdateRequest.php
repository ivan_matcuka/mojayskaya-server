<?php

namespace App\Http\Requests;

use App\CandidateApplication;
use App\Vacancy;
use App\VacancyApplication;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Http\FormRequest;

class CandidateApplicationUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status' => 'int|required',
        ];
    }

    public function persist()
    {
        try {
            $candidateApplication = CandidateApplication::query()
                ->findOrFail($this->route('id'));

            $candidateApplication->update([
                'status' => (int)$this->get('status')
            ]);

            $candidateApplication->load(['user', 'candidate']);

            return response()->json($candidateApplication);
        } catch (ModelNotFoundException $exception) { // Catching 404s
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(404);
        } catch (Exception $exception) { // Catching other errors
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(500);
        }
    }
}
