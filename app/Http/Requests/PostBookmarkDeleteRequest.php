<?php

namespace App\Http\Requests;

use App\PostBookmark;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class PostBookmarkDeleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'user_id' => 'integer|required',
            'post_id' => 'integer|required',
        ];
    }

    /**
     * Persists the request.
     *
     * @return JsonResponse
     */
    public function persist(): JsonResponse
    {
        try {
            $postBookmark = PostBookmark::query()
                ->where([
                    ['user_id', '=', $this->get('user_id')],
                    ['post_id', '=', $this->get('post_id')],
                ])
                ->firstOrFail();

            $postBookmark->delete();

            return response()->json($postBookmark->load('post'));
        } catch (ModelNotFoundException $exception) { // Catching 404s
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(404);
        } catch (Exception $exception) { // Catching other errors
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(500);
        }
    }
}
