<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\SoftDeletes;

class Poll extends Model
{
    use SoftDeletes;

    protected $fillable = ['subject', 'teaser', 'active', 'start', 'end', 'created_at'];

    protected $appends = ['is_answered', 'type'];

    /**
     * Returns the poll options.
     *
     * @return HasMany
     */
    public function options(): HasMany
    {
        return $this->hasMany('App\Option');
    }

    public function responses(): HasManyThrough
    {
        return $this->hasManyThrough('App\PollResponse', 'App\Option');
    }

    public function getIsAnsweredAttribute(): bool
    {
        return $this->responses()
            ->pluck('user_id')
            ->contains(auth()->id());
    }

    public function getTypeAttribute(): string
    {
        return 'poll';
    }
}
