<?php

namespace App\Http\Requests;

use App\Poll;
use App\Post;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Http\FormRequest;

class PostDeleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function persist() {
        /**
         * Trying to find the post
         * and delete it.
         */
        try {
            $post = Post::query()->findOrFail($this->route('id'));

            return response()->json($post->delete());
        } catch (ModelNotFoundException $exception) { // Catching 404s
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(404);
        } catch (Exception $exception) { // Catching other errors
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(500);
        }
    }
}
