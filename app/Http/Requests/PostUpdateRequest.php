<?php

namespace App\Http\Requests;

use App\Option;
use App\Poll;
use App\Post;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Http\FormRequest;

class PostUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'subject' => 'required|string',
//            'teaser' => 'required|string',
//            'illustration' => 'mimes:jpeg,jpg,png,gif',
//            'content' => 'required|string',
        ];
    }

    public function persist()
    {
        /**
         * Trying to find the poll
         * and delete it.
         */
        try {
            /** @var Post $post */
            $post = Post::query()->findOrFail($this->route('id'));
            return response()->json($post->update($this->all()));
        } catch (ModelNotFoundException $exception) { // Catching 404s
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(404);
        } catch (Exception $exception) { // Catching other errors
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(500);
        }
    }
}
