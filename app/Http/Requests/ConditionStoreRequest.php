<?php

namespace App\Http\Requests;

use App\Condition;
use App\Post;
use Exception;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class ConditionStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
        ];
    }

    /**
     * Persists the request.
     *
     * @return JsonResponse
     */
    public function persist(): JsonResponse
    {
        try {
            $condition = new Condition($this->all());
            $condition->save();

            return response()->json($condition);
        } catch (Exception $exception) {
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(500);
        }
    }
}
