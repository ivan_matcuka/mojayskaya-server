<?php

namespace App\Http\Requests;

use App\Event;
use App\EventSettings;
use App\Option;
use App\Poll;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Http\FormRequest;

class EventUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date_time' => 'required|date',
            'subject' => 'required|string',
            'teaser' => 'required|string',
            'illustration' => 'mimes:jpeg,jpg,png,gif',
            'description' => 'required|string',
            'address' => 'required|string',
            'limit' => 'required|integer',
            'type' => 'required|integer',
            'valid_til' => 'date',
        ];
    }

    public function persist()
    {

        /**
         * Trying to find the poll
         * and delete it.
         */
        try {
            /** @var Event $event */
            $event = Event::query()->findOrFail($this->route('id'));

            if (count($this->get('experts'))) {
                $event->experts()->detach();

                foreach ($this->get('experts') as $expertId) {
                    /** @var User $expert */
                    $expert = User::query()->findOrFail($expertId);

                    $expert->timestamps = false;
                    $event->experts()->attach([
                        $expert->getAttribute('id') => ['role' => 1],
                    ]);
                }
            }

            $validTil = $this->get('valid_til') ?? $this->get('date_time');

            $settings = $event->getAttribute('settings');
            $settings->timestamps = false;
            $settings->update([
                'limit' => $this->get('limit'),
                'type' => $this->get('type'),
                'valid_til' => Carbon::parse($validTil),
            ]);

            $event->update([
                'date_time' => Carbon::parse($this->get('date_time')),
                'subject' => $this->get('subject'),
                'teaser' => $this->get('teaser'),
                'illustration' => $this->file('illustration'),
                'description' => $this->get('description'),
                'address' => $this->get('address'),
            ]);

            return response()->json($event->load(['experts', 'settings', 'users']));
        } catch (ModelNotFoundException $exception) { // Catching 404s
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(404);
        } catch (Exception $exception) { // Catching other errors
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(500);
        }
    }
}
