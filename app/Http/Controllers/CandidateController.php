<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\Condition;
use App\Http\Requests\CandidateDeleteRequest;
use App\Http\Requests\CandidateFavoriteRequest;
use App\Http\Requests\CandidateStoreRequest;
use App\Http\Requests\CandidateUpdateRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CandidateController extends Controller
{
    public function all()
    {
        return response()->json(
            Candidate::all()
                ->load(['conditions', 'user'])
                ->sortBy('id')
                ->values()
        );
    }

    public function one()
    {
        try {
            return response()->json(
                Candidate::query()->findOrFail(request('id'))
                    ->load(['applications', 'conditions'])
            );
        } catch (ModelNotFoundException $exception) {
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(404);
        }

    }

    public function store(CandidateStoreRequest $request)
    {
        return $request->persist();
    }

    public function delete(CandidateDeleteRequest $request)
    {
        return $request->persist();
    }

    public function update(CandidateUpdateRequest $request)
    {
        return $request->persist();
    }

    public function addToFavorite(CandidateFavoriteRequest $request)
    {
        return $request->persist();
    }
}
