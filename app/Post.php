<?php

namespace App;

// Framework
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;

    protected $fillable = ['subject', 'teaser', 'illustration', 'content', 'created_at'];

    protected $appends = ['is_bookmark', 'type'];

    /**
     * Returns the bookmarks of this post.
     *
     * @return HasMany
     */
    public function bookmarks(): HasMany
    {
        return $this->hasMany('App\PostBookmark');
    }

    /**
     * Gets the "is_bookmark" field.
     *
     * @return bool
     */
    public function getIsBookmarkAttribute(): bool
    {
        return $this->bookmarks()
            ->pluck('user_id')
            ->contains(auth()->id());
    }

    /**
     * Gets the "type" field.
     *
     * @return string
     */
    public function getTypeAttribute(): string
    {
        return 'post';
    }
}
