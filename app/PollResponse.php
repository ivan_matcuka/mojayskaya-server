<?php

namespace App;

// Framework
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class PollResponse extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id', 'option_id'];

    /**
     * Returns the user who sent
     * this response.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Returns the options this response
     * is addressed to.
     *
     * @return BelongsTo
     */
    public function option(): BelongsTo
    {
        return $this->belongsTo('App\Option');
    }
}
