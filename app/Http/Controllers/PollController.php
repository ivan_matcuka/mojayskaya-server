<?php

namespace App\Http\Controllers;

use App\Poll;
use App\Http\Requests\PollDeleteRequest;
use App\Http\Requests\PollStoreRequest;
use App\Http\Requests\PollUpdateRequest;
use Illuminate\Http\JsonResponse;

class PollController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Deletes the poll.
     *
     * @param PollDeleteRequest $request - Request object
     * @return JsonResponse
     */
    public function delete(PollDeleteRequest $request)
    {
        return $request->persist();
    }

    public function update(PollUpdateRequest $request)
    {
        return $request->persist();
    }

    /**
     * Fetches all polls.
     *
     * @return JsonResponse
     */
    public function all()
    {
        return response()->json(
            Poll::all()
                ->load(['options', 'responses'])
        );
    }

    /**
     * Stores a poll and its options to
     * the database.
     *
     * @param PollStoreRequest $request - Request object
     * @return JsonResponse
     */
    public function store(PollStoreRequest $request)
    {
        return $request->persist();
    }
}
