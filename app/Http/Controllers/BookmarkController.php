<?php

namespace App\Http\Controllers;

use App\Http\Requests\EventBookmarkDeleteRequest;
use App\Http\Requests\EventBookmarkStoreRequest;
use App\Http\Requests\PostBookmarkDeleteRequest;
use App\Http\Requests\PostBookmarkStoreRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

// Models
use App\Event;
use App\Post;

class BookmarkController extends Controller
{
    public function all(): JsonResponse {
        $events = Event::all();
        $posts = Post::all();

        $collection = collect([$events, $posts])->flatten()->filter(static function ($object, $_) {
            return $object->isBookmark;
        });

        return response()->json($collection->values());
    }

    public function storeEventBookmark(EventBookmarkStoreRequest $request): JsonResponse
    {
        return $request->persist();
    }

    public function deleteEventBookmark(EventBookmarkDeleteRequest $request): JsonResponse
    {
        return $request->persist();
    }

    public function storePostBookmark(PostBookmarkStoreRequest $request): JsonResponse
    {
        return $request->persist();
    }

    public function deletePostBookmark(PostBookmarkDeleteRequest $request): JsonResponse
    {
        return $request->persist();
    }
}
