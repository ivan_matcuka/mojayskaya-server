<?php

namespace App;

// Framework
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Option extends Model
{
    protected $fillable = ['text'];

    /**
     * Returns the poll this option
     * is attached to.
     *
     * @return BelongsTo
     */
    public function poll(): BelongsTo
    {
        return $this->belongsTo('App\Poll');
    }

    /**
     * Returns the poll responses this
     * option is selected in.
     *
     * @return BelongsToMany
     */
    public function pollResponses(): BelongsToMany
    {
        return $this->belongsToMany('App\PollResponse');
    }
}
