<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(CandidatesTableSeeder::class);
        $this->call(ConditionsTableSeeder::class);
        $this->call(VacanciesTableSeeder::class);
        $this->call(EventsTableSeeder::class);
        $this->call(PollsTableSeeder::class);
        $this->call(PostsTableSeeder::class);
        $this->call(OptionsTableSeeder::class);
    }
}
