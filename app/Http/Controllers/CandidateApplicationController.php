<?php

namespace App\Http\Controllers;

use App\CandidateApplication;
use App\Http\Requests\CandidateApplicationDeleteRequest;
use App\Http\Requests\CandidateApplicationStoreRequest;
use App\Http\Requests\CandidateApplicationUpdateRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CandidateApplicationController extends Controller
{
    public function all()
    {
        return response()->json(CandidateApplication::all()->load(['user', 'candidate']));
    }

    public function one()
    {
        try {
            return response()->json(
                CandidateApplication::query()
                    ->findOrFail(request('id'))
                    ->load(['user', 'candidate'])
            );
        } catch (ModelNotFoundException $exception) {
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(404);
        }

    }

    public function store(CandidateApplicationStoreRequest $request)
    {
        return $request->persist();
    }

    public function delete(CandidateApplicationDeleteRequest $request)
    {
        return $request->persist();
    }

    public function update(CandidateApplicationUpdateRequest $request)
    {
        return $request->persist();
    }
}
