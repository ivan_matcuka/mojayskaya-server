<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CandidatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        DB::table('candidates')->insert([
            'position' => 'SEO-специалист',
            'salary' => '50000',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eu nunc congue, cursus ex eget, maximus nibh. Duis accumsan facilisis congue. Praesent pharetra posuere est, eu euismod quam ultrices vel. Praesent suscipit cursus risus eget cursus. Duis id est rhoncus, malesuada justo ut, imperdiet nisl. Ut velit purus, mattis sed imperdiet non, laoreet id lectus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Phasellus venenatis libero magna, nec mollis lectus porta et. Nunc et condimentum lorem. Integer semper odio ipsum, eu rutrum libero suscipit vitae. Aliquam erat volutpat.',
            'for_whom' => 'Ut ut dictum sem. Praesent venenatis neque sit amet porttitor semper. Aliquam pretium, felis non tristique molestie, diam est vulputate risus, id pulvinar risus arcu placerat arcu. Curabitur molestie augue sed vestibulum gravida. Suspendisse sapien ligula, cursus quis nunc sit amet, mattis viverra leo. Pellentesque nec odio libero. Maecenas pharetra magna nec orci ultrices tristique. Donec nisi ante, rutrum sed nulla sed, vestibulum suscipit arcu. Cras facilisis aliquam commodo. Nam ut nibh vel risus congue mollis.',
            'skills' => 'Donec finibus massa eget tristique laoreet. Quisque vulputate id urna et laoreet. Nam dignissim a dolor quis dictum. Aliquam commodo mauris sit amet fermentum eleifend. Vivamus euismod nulla erat, ut euismod erat viverra eu. Praesent mattis et velit id luctus. Nam non erat dolor.',
            'created_at' => $faker->dateTimeBetween('-6 days', '-3 days'),
        ]);
        DB::table('candidates')->insert([
            'position' => 'Водитель',
            'salary' => '25000',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eu nunc congue, cursus ex eget, maximus nibh. Duis accumsan facilisis congue. Praesent pharetra posuere est, eu euismod quam ultrices vel. Praesent suscipit cursus risus eget cursus. Duis id est rhoncus, malesuada justo ut, imperdiet nisl. Ut velit purus, mattis sed imperdiet non, laoreet id lectus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Phasellus venenatis libero magna, nec mollis lectus porta et. Nunc et condimentum lorem. Integer semper odio ipsum, eu rutrum libero suscipit vitae. Aliquam erat volutpat.',
            'for_whom' => 'Ut ut dictum sem. Praesent venenatis neque sit amet porttitor semper. Aliquam pretium, felis non tristique molestie, diam est vulputate risus, id pulvinar risus arcu placerat arcu. Curabitur molestie augue sed vestibulum gravida. Suspendisse sapien ligula, cursus quis nunc sit amet, mattis viverra leo. Pellentesque nec odio libero. Maecenas pharetra magna nec orci ultrices tristique. Donec nisi ante, rutrum sed nulla sed, vestibulum suscipit arcu. Cras facilisis aliquam commodo. Nam ut nibh vel risus congue mollis.',
            'skills' => 'Donec finibus massa eget tristique laoreet. Quisque vulputate id urna et laoreet. Nam dignissim a dolor quis dictum. Aliquam commodo mauris sit amet fermentum eleifend. Vivamus euismod nulla erat, ut euismod erat viverra eu. Praesent mattis et velit id luctus. Nam non erat dolor.',
            'created_at' => $faker->dateTimeBetween('-6 days', '-3 days'),
        ]);
        DB::table('candidates')->insert([
            'position' => 'Мендежер проектов',
            'salary' => '90000',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eu nunc congue, cursus ex eget, maximus nibh. Duis accumsan facilisis congue. Praesent pharetra posuere est, eu euismod quam ultrices vel. Praesent suscipit cursus risus eget cursus. Duis id est rhoncus, malesuada justo ut, imperdiet nisl. Ut velit purus, mattis sed imperdiet non, laoreet id lectus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Phasellus venenatis libero magna, nec mollis lectus porta et. Nunc et condimentum lorem. Integer semper odio ipsum, eu rutrum libero suscipit vitae. Aliquam erat volutpat.',
            'for_whom' => 'Ut ut dictum sem. Praesent venenatis neque sit amet porttitor semper. Aliquam pretium, felis non tristique molestie, diam est vulputate risus, id pulvinar risus arcu placerat arcu. Curabitur molestie augue sed vestibulum gravida. Suspendisse sapien ligula, cursus quis nunc sit amet, mattis viverra leo. Pellentesque nec odio libero. Maecenas pharetra magna nec orci ultrices tristique. Donec nisi ante, rutrum sed nulla sed, vestibulum suscipit arcu. Cras facilisis aliquam commodo. Nam ut nibh vel risus congue mollis.',
            'skills' => 'Donec finibus massa eget tristique laoreet. Quisque vulputate id urna et laoreet. Nam dignissim a dolor quis dictum. Aliquam commodo mauris sit amet fermentum eleifend. Vivamus euismod nulla erat, ut euismod erat viverra eu. Praesent mattis et velit id luctus. Nam non erat dolor.',
            'created_at' => $faker->dateTimeBetween('-6 days', '-3 days'),
        ]);
    }
}
