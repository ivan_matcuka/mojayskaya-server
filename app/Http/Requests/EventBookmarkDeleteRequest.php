<?php

namespace App\Http\Requests;

use App\EventBookmark;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class EventBookmarkDeleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'user_id' => 'integer|required',
            'event_id' => 'integer|required',
        ];
    }

    /**
     * Persists the request.
     *
     * @return JsonResponse
     */
    public function persist(): JsonResponse
    {
        try {
            $eventBookmark = EventBookmark::query()
                ->where([
                    ['user_id', '=', $this->get('user_id')],
                    ['event_id', '=', $this->get('event_id')],
                ])
                ->firstOrFail();

            $eventBookmark->delete();

            return response()->json($eventBookmark->load(['event.experts', 'event.settings', 'event.users']));
        } catch (ModelNotFoundException $exception) { // Catching 404s
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(404);
        } catch (Exception $exception) { // Catching other errors
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(500);
        }
    }
}
