<?php

namespace App\Http\Requests;

use App\Condition;
use App\Option;
use App\Poll;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Http\FormRequest;

class ConditionUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'string|required',
        ];
    }

    public function persist()
    {
        /**
         * Trying to find the poll
         * and delete it.
         */
        try {
            /** @var Poll $poll */
            $condition = Condition::query()->findOrFail($this->route('id'));
            $condition->update(['title' => $this->get('title')]);

            return response()->json($condition);
        } catch (ModelNotFoundException $exception) { // Catching 404s
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(404);
        } catch (Exception $exception) { // Catching other errors
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(500);
        }
    }
}
