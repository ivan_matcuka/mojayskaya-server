<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\Event;
use App\Poll;
use App\Post;
use App\User;
use App\Vacancy;
use Illuminate\Database\Eloquent\Collection;

class FeedController extends Controller
{
    public function all($page = 1)
    {
        /** @var User $user */
        $user = auth()->user();

        $posts = Post::all();
        $events = Event::all();
        $candidates = Candidate::all();
        $vacancies = Vacancy::all();
        $polls = Poll::all()->load(['options', 'responses']);

        $collection = collect([$posts, $events, $candidates, $vacancies, $polls]);

        return response()->json(
            $collection
                ->flatten()
                ->sortByDesc('created_at')
                ->values()
        );
    }
}
