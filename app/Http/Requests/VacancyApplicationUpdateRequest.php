<?php

namespace App\Http\Requests;

use App\Vacancy;
use App\VacancyApplication;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Http\FormRequest;

class VacancyApplicationUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status' => 'int|required',
        ];
    }

    public function persist()
    {
        try {
            $vacancyApplication = VacancyApplication::query()
                ->findOrFail($this->route('id'));

            $vacancyApplication->update([
                'status' => (int)$this->get('status')
            ]);

            $vacancyApplication->load(['user', 'vacancy']);

            return response()->json($vacancyApplication);
        } catch (ModelNotFoundException $exception) { // Catching 404s
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(404);
        } catch (Exception $exception) { // Catching other errors
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(500);
        }
    }
}
