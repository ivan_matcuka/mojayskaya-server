<?php

namespace App\Http\Requests;

use App\Candidate;
use App\CandidateApplication;
use App\Condition;
use App\User;
use App\Vacancy;
use App\VacancyApplication;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Http\FormRequest;

class CandidateApplicationStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status' => 'int|required',
            'user_id' => 'int|required',
            'candidate_id' => 'int|required',
        ];
    }

    public function persist()
    {
        try {
            $candidateApplication = CandidateApplication::query()
                ->where([
                    ['user_id', '=', $this->get('user_id')],
                    ['candidate_id', '=', $this->get('candidate_id')],
                ])
                ->first();

            if ($candidateApplication) {
                throw new Exception('You already applied!');
            }

            $user = User::query()->findOrFail($this->get('user_id'));
            $candidate = Candidate::query()->findOrFail($this->get('candidate_id'));

            $candidateApplication = new CandidateApplication([
                'status' => (int)$this->get('status'),
            ]);

            $candidateApplication->user()->associate($user);
            $candidateApplication->candidate()->associate($candidate);

            $candidateApplication->save();

            /**
             * Options eager loading
             */
            $candidateApplication->load(['user', 'candidate.applications', 'candidate.conditions']);

            return response()->json($candidateApplication);
        } catch (ModelNotFoundException $exception) {
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(404);
        } catch (Exception $exception) {
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(500);
        }
    }
}
