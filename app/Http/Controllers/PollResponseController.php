<?php

namespace App\Http\Controllers;

use App\Http\Requests\PollResponseStoreRequest;
use Illuminate\Http\JsonResponse;

class PollResponseController extends Controller
{
    /**
     * Stores a poll response.
     *
     * @param PollResponseStoreRequest $request
     * @return JsonResponse
     */
    public function store(PollResponseStoreRequest $request): JsonResponse
    {
        return $request->persist();
    }
}
