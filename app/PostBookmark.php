<?php

namespace App;

// Framework
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class PostBookmark extends Model
{
    public $timestamps = false;

    public $fillable = ['user_id', 'post_id'];

    /**
     * Returns the user who created the
     * bookmark.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Returns the post attached to the
     * bookmark.
     *
     * @return BelongsTo
     */
    public function post(): BelongsTo
    {
        return $this->belongsTo('App\Post', 'post_id');
    }
}
