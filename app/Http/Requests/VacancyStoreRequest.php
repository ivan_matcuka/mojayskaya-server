<?php

namespace App\Http\Requests;

use App\Candidate;
use App\Condition;
use App\Option;
use App\Poll;
use App\User;
use App\Vacancy;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Http\FormRequest;

class VacancyStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'position' => 'required|string',
            'salary' => 'required|string',
            'description' => 'required|string',
            'tasks' => 'required|string',
            'requirements' => 'required|string',
            'conditions' => 'array',
        ];
    }

    public function persist()
    {
        try {
            /**
             * Create a new poll
             */
            $vacancy = new Vacancy([
                'position' => $this->get('position'),
                'salary' => $this->get('salary'),
                'description' => $this->get('description'),
                'tasks' => $this->get('tasks'),
                'requirements' => $this->get('requirements'),
            ]);

            /**
             * Save it as we need its ID in order
             * to attach conditions
             */
            $vacancy->save();

            foreach ($this->get('conditions') as $conditionId) {
                $condition = Condition::query()->findOrFail($conditionId);

                if ($condition) {
                    $vacancy->conditions()->attach($conditionId);
                }
            }

            /**
             * Options eager loading
             */
            $vacancy->load('conditions');

            return response()->json($vacancy);
        } catch (ModelNotFoundException $exception) {
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(404);
        } catch (Exception $exception) {
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(500);
        }
    }
}
