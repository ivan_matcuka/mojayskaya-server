<?php

namespace App\Http\Controllers;

use App\Http\Requests\VacancyApplicationDeleteRequest;
use App\Http\Requests\VacancyApplicationStoreRequest;
use App\Http\Requests\VacancyApplicationUpdateRequest;
use App\Vacancy;
use App\VacancyApplication;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class VacancyApplicationController extends Controller
{
    public function all()
    {
        return response()->json(VacancyApplication::all()->load(['user', 'vacancy']));
    }

    public function one()
    {
        try {
            return response()->json(
                VacancyApplication::query()
                    ->findOrFail(request('id'))
                    ->load(['user', 'vacancy'])
            );
        } catch (ModelNotFoundException $exception) {
            return response()
                ->json(['message' => $exception->getMessage()])
                ->setStatusCode(404);
        }

    }

    public function store(VacancyApplicationStoreRequest $request)
    {
        return $request->persist();
    }

    public function delete(VacancyApplicationDeleteRequest $request)
    {
        return $request->persist();
    }

    public function update(VacancyApplicationUpdateRequest $request)
    {
        return $request->persist();
    }
}
