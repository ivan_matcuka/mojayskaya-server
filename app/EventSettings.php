<?php

namespace App;

// Framework
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class EventSettings extends Model
{
    public $fillable = [
        'limit',
        'type',
        'valid_til',
    ];

    /**
     * Returns the event described
     * by these settings.
     *
     * @return HasOne
     */
    public function event(): HasOne
    {
        return $this->hasOne('App\Event');
    }
}
